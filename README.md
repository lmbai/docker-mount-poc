# Test how the docker mount work

## Prepare the docker image
```
docker build -t app .
```

## summary : 	

- if the mapping type is binding, it will use the source version		
- if the mapping type is volume, 
    - if the source folder is empty, 
        - if we don't have the nocopy setup, it will try to copy the existing file (if exisits) from the target folder to the source folder 		
        - if we have the nocopy setup, it will not copy the existing file (if exisits) from the target folder to the source folder 	
    - if the source folder is not empty, it will user the source folder as the target folder, (all the existing files in the existing target folder is been hided.)	

> all the above copy or no copy case only happen during the container creation stage. after the container startup, any file change will affect both source and target folder.		
